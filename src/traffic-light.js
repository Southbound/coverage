function getTrafficHexColor(word) {
    if (word == "RED") {
        return "#FF0000";
    } else if (word == "YELLOW") {
        return "#FFFF00";
    } else if (word == "GREEN") {
        return "#00FF00";
    } else {
        return false;
    }
}
module.exports = 
{
    getTrafficHexColor
};