const {
    getTrafficHexColor
} = require("../src/traffic-light");

const readline = require("readline")
const rl =
    readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })
let userInput = "";

function main() {
    rl.question("\n" + "RED / YELLOW / GREEN ?\n", function (userInput) {
        if (userInput == 'QUIT') {
            rl.close();
            console.log("- Program exited.");
            process.exit();
        } else if (getTrafficHexColor(userInput) === false) {
            console.log("- Invalid input. Use only capital letters.");
            main();
        } else {
            console.log(`- Traffic light toggled to: ${userInput}.`);
            main();


        }

    })
}

console.log("- Input QUIT to exit program.")
main()